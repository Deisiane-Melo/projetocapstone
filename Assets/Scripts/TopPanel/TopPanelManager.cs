using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TopPanelManager : MonoBehaviour 
{
    [SerializeField] public GameObject TopPanel;
    [SerializeField] public GameObject GameOver;
    [SerializeField] public GameObject PauseMenu;
    [SerializeField] public GameObject NameSave;
    [SerializeField] private string gameScene;
    [SerializeField] private string Menu;

    public void OpenPause() 
    {
        Time.timeScale = 0;
        PauseMenu.SetActive(true);
        TopPanel.SetActive(false);
    }
    public void ClosePause() 
    {
        Time.timeScale = 1;
        PauseMenu.SetActive(false);
        TopPanel.SetActive(true);
    }
    public void OpenPauseGameOver()
    {
        Time.timeScale = 0;
        GameOver.SetActive(true);
        TopPanel.SetActive(false);
    }

    public void ClosePauseGameOver()
    {
        Time.timeScale = 1;
        PauseMenu.SetActive(false);
        SceneManager.LoadScene(gameScene);
    }

    public void RetunToMainMenu()
    {
        Time.timeScale = 0;
        PauseMenu.SetActive(false);
        SceneManager.LoadScene(gameScene);

    }

    public void OpenSaveName()
    {
        Time.timeScale = 0;
        NameSave.SetActive(true);
        TopPanel.SetActive(false);
        GameOver.SetActive(false);
    }
    public void CloseSaveName()
    {
        NameSave.SetActive(false);
        SceneManager.LoadScene(gameScene);
    }
}
