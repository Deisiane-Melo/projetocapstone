using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Player : MonoBehaviour
{
    public ShipStats shipStats;
    public GameObject bulletPrefab;
    public AudioSource audioLaser;

    private const float MAX_LEFT = -3.26f;
    private const float MAX_RIGHT = 3.89f;
    private const float SHOOT_COOLDOWN = 0.5f;

    private bool isShooting;
    private float shootTimer;

    private TopPanelManager topPanelManager;

    private void Start()
    {
        topPanelManager = FindObjectOfType<TopPanelManager>();

        shipStats.currentHealth = shipStats.maxHealth;
        shipStats.currentLives = shipStats.maxLives;

        UIManager.UpdateHeallthbar(shipStats.currentHealth, shipStats.maxHealth);
        UIManager.UpdateLives(shipStats.currentLives);
    }

    private void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float movement = horizontalInput * Time.deltaTime * shipStats.shipSpeed;

        // Movimenta��o horizontal
        transform.Translate(Vector2.right * movement);

        // Limitar a posi��o do jogador dentro dos limites do jogo
        Vector3 clampedPosition = transform.position;
        clampedPosition.x = Mathf.Clamp(clampedPosition.x, MAX_LEFT, MAX_RIGHT);
        transform.position = clampedPosition;

        // Disparo
        if (Input.GetKeyDown(KeyCode.Space) && !isShooting && shootTimer <= 0)
        {
            StartCoroutine(Shoot());
            audioLaser.Play();
            shootTimer = SHOOT_COOLDOWN;
        }

        // Atualizar o temporizador de disparo
        if (shootTimer > 0)
        {
            shootTimer -= Time.deltaTime;
        }
    }

    private IEnumerator Shoot()
    {
        isShooting = true;
        Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        yield return new WaitForSeconds(shipStats.fireReate);
        isShooting = false;
    }

    public void LivePlus()
    {
        shipStats.currentHealth++;
        UIManager.UpdateHeallthbar(shipStats.currentHealth, shipStats.maxHealth);

        if (shipStats.currentLives < 7)
        {
            shipStats.currentLives++;
            UIManager.UpdateLives(shipStats.currentLives);
        }
    }

    private void TakeDamage()
    {
        shipStats.currentHealth--;
        UIManager.UpdateHeallthbar(shipStats.currentHealth, shipStats.maxHealth); // Passando tamb�m o valor de maxHealth

        if (shipStats.currentHealth <= 0)
        {
            shipStats.currentLives--;
            UIManager.UpdateLives(shipStats.currentLives);

            if (shipStats.currentLives <= 0)
            {
                if (topPanelManager != null)
                {
                    topPanelManager.OpenPauseGameOver();
                    SaveManager.SaveProgress();
                }
                else
                {
                    Debug.LogError("topPanelManager n�o encontrado na cena.");
                }
            }
            else
            {
                StartCoroutine(Respawn());
            }
        }
    }


    private IEnumerator Respawn()
    {
        // Implemente a l�gica de respawn aqui
        yield return null;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("EnemyBullet"))
        {
            TakeDamage();
            Destroy(collision.gameObject);
        }

    }
}