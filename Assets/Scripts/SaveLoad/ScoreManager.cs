using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager Instance { get; private set; }

    private List<ScoreEntry> scoreEntries = new List<ScoreEntry>();
    private string saveFilePath;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        // Defina o caminho do arquivo de salvamento dentro da pasta Assets
        saveFilePath = Path.Combine(Application.dataPath, "scores.json");

        // Carregue as pontuações salvas do arquivo
        LoadScores();
    }

    public List<ScoreEntry> GetSavedScores()
    {
        return scoreEntries;
    }

    public void AddScore(string playerName, int score)
    {
        // Inclua o nome do jogador junto com a pontuação
        scoreEntries.Add(new ScoreEntry(playerName, score));
        SaveScores();
    }

    private void SaveScores()
    {
        // Converta as pontuações para JSON
        string json = JsonUtility.ToJson(scoreEntries);

        // Escreva o JSON no arquivo
        File.WriteAllText(saveFilePath, json);
    }

    private void LoadScores()
    {
        // Verifique se o arquivo de salvamento existe
        if (File.Exists(saveFilePath))
        {
            // Leia o JSON do arquivo
            string json = File.ReadAllText(saveFilePath);

            // Converta o JSON de volta para uma lista de pontuações
            scoreEntries = JsonUtility.FromJson<List<ScoreEntry>>(json);
        }
    }
}
