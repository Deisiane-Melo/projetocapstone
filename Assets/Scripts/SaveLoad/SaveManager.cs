using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public GameObject ship;
    public static SaveManager instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        LoadProgress();
    }

    public static void SaveProgress()
    {
        SaveObject so = new SaveObject();
        so.highscore = UIManager.GetHighScore();
        Player player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        so.shipStats = player.shipStats;
    }

    public static void LoadProgress()
    {
        SaveObject so = SaveLoad.LoadState();

        UIManager.UpdateHighscore(so.highscore);

        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().shipStats = so.shipStats;

    }
}
