using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ScoreHistoryPanel : MonoBehaviour
{
    public Text scoreHistoryText;
    //public ScoreManager scoreManager;

    private void Start()
    {
        // Caminho para o arquivo dentro do diret�rio "Assets"
        string filePath = Path.Combine(Application.dataPath, "pontuacoes");

        // Verifica se o arquivo existe
        if (File.Exists(filePath))
        {
            // L� o conte�do do arquivo
            string[] lines = File.ReadAllLines(filePath);

            // Atualiza o texto do painel com o conte�do do arquivo
            scoreHistoryText.text = string.Join("\n", lines);
        }
        else
        {
            Debug.LogWarning("O arquivo de pontua��es n�o existe.");
        }
    //    UpdateScoreHistoryUI();
    }
    /*
    private void UpdateScoreHistoryUI()
    {
        List<ScoreEntry> savedScores = scoreManager.GetSavedScores();

        // Limpe o texto atual antes de adicionar as novas pontua��es
        scoreHistoryText.text = "";

        foreach (ScoreEntry scoreEntry in savedScores)
        {
            // Adicione o nome do jogador e a pontua��o ao texto
            scoreHistoryText.text += scoreEntry.playerName + ": " + scoreEntry.score + "\n";
        }
    }*/
}
