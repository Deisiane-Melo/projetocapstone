using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public GameObject gameOverPanel;
    public GameObject nameInputPanel;
    public InputField playerNameInput;
    public GameObject scoreHistoryPanel;

    private int finalScore;

    public void ShowGameOverPanel(int score)
    {
        finalScore = score;
        gameOverPanel.SetActive(true);
    }

    public void SaveScoreAndShowNameInputPanel()
    {
        gameOverPanel.SetActive(false);
        nameInputPanel.SetActive(true);
    }

    public void SaveScoreWithPlayerName()
    {
        string playerName = playerNameInput.text;
        ScoreManager.Instance.AddScore(playerName, finalScore);

        nameInputPanel.SetActive(false);
        scoreHistoryPanel.SetActive(true);

        // Atualize a tela de hist�rico de pontua��es
        // Aqui voc� deve chamar um m�todo para atualizar a UI com as pontua��es salvas
    }
}