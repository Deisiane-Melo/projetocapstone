

[System.Serializable]
public class SaveObject
{
    public int highscore;
    public ShipStats shipStats;

    public SaveObject()
    {
        highscore = 0;

        shipStats = new ShipStats();

        shipStats.maxHealth = 3;
        shipStats.maxLives = 3;
        shipStats.shipSpeed = 3;
        shipStats.fireReate = 0.5f;
    }
}
