using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    private int score;

    public TextMeshProUGUI highscoreText;
    private int highscore;

    public TextMeshProUGUI waveText;
    private int wave;

    public Image[] lifeSprites;
    public Image healthBar;

    private Color32 active = new Color(1, 1, 1, 1);
    private Color32 inactive = new Color(1, 1, 1, 0.25f);

    private static UIManager instance;
    private Player player;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
        player = FindObjectOfType<Player>();

    }

    public static void UpdateLives(int l)
    {
        foreach (Image i in instance.lifeSprites)
            i.color = instance.inactive;

        for (int i = 0; i < l; i++)
        {
            instance.lifeSprites[i].color = instance.active;
        }
    }

    public static void UpdateHeallthbar(int currentHealth, int maxHealth)
    {
        float healthPercentage = (float)currentHealth / maxHealth;

        instance.healthBar.fillAmount = healthPercentage;
    }

    public static void UpdateScore(int s, Player player)
    {
        instance.score += s;
        instance.scoreText.text = instance.score.ToString("000.00");

        if (instance.score % 1000 == 0)
        {
            player.LivePlus();
        }
    }

    public static void ResetUI()
    {
        instance.score = 0;
        instance.wave = 0;
        instance.scoreText.text = instance.score.ToString("000,00");
        instance.waveText.text = instance.wave.ToString();
    }

    public static void UpdateHighscore(int hs)
    {
        if (instance.highscore < hs)
        {
            instance.highscore = hs;
            instance.highscoreText.text = instance.highscore.ToString("000.00");
        }
    }

    public static void UpdateWave()
    {
        instance.wave++;
        instance.waveText.text = instance.wave.ToString();
    }

    public static int GetHighScore()
    {
        return instance.highscore;
    }
}
