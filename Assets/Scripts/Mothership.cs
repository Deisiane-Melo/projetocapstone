using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class Mothership : MonoBehaviour
{

    public int scoreValue;

    private const float MAX_LEFT = -9f;
    private float speed = 5;
    
    void Update()
    {
        transform.Translate(Vector2.left * Time.deltaTime * speed);

        if (transform.position.x <= MAX_LEFT)
            Destroy(gameObject);

        
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("FriendlyBullet"))
        {
            UIManager.UpdateScore(scoreValue, FindObjectOfType<Player>());
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }

    }
}
