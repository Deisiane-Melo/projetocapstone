using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private string gameScene;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject MainMenuPanel;
    [SerializeField] public GameObject TopPanel;
    [SerializeField] public GameObject HistoricoScore;
    [SerializeField] private GameObject tutorialMenu;

    public void Play()
    {
        Time.timeScale = 1;
        Debug.Log("Começou o jogo");
        SceneManager.LoadScene(gameScene);
        mainMenu.SetActive(false);
    }

    public void OpenOptions()
    {
        Debug.Log("Abriu opções do jogo");
        MainMenuPanel.SetActive(false);
        tutorialMenu.SetActive(true);
    }

    public void CloseOptions()
    {
        Debug.Log("Fechou opções do jogo");
        MainMenuPanel.SetActive(true);
        tutorialMenu.SetActive(false);
    }



    public void ExitGame()
    {
        Debug.Log("Saindo do jogo");
        Application.Quit();
    }

    public void OpenHistorico()
    {
        HistoricoScore.SetActive(true);
        MainMenuPanel.SetActive(false);
        TopPanel.SetActive(false);
    }
    public void CloseHistorico()
    {
        HistoricoScore.SetActive(false);
        MainMenuPanel.SetActive(true);

    }
}